import Vue from 'vue'
import Vuex from 'vuex'
import {ADD_NOTE, GET_NOTES, SAVE_NOTES, SET_NOTES} from "@/store/types";
import {getDate, getNotesFromLocalStorage, saveNotesToLocalStorage} from "@/helpers";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        note: {},
        notes: []
    },
    mutations: {
        [ADD_NOTE]: (state, noteData) => {
            const note = {
                title: noteData.title,
                createdAt: getDate(),
                updatedAt: getDate(),
                content: noteData.content
            };
            state.notes.push(note)
        },
        [SET_NOTES]: (state, notes) => state.notes = notes
    },
    actions: {
        [GET_NOTES]({commit}) {
            const notes = getNotesFromLocalStorage()
            notes.length ? commit(SET_NOTES, notes) : commit(SET_NOTES, [])
        },
        [SAVE_NOTES]({state}) {
            saveNotesToLocalStorage(state.notes)
        }
    },
    modules: {}
})
