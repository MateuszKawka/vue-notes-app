export const getDate = () => new Date().toISOString

export const saveNotesToLocalStorage = (notes) => {
    localStorage.set('notes', JSON.stringify(notes))
}

export const getNotesFromLocalStorage = () => {
    if (localStorage.get('notes')) {
        return JSON.parse(localStorage.get('notes'))
    }
}