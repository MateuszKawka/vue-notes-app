import Vue from 'vue'
import VueRouter from 'vue-router'
import NotesView from '../views/NotesView.vue'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Notes',
        component: NotesView
    },
    {
        path: '/note/:id?',
        name: 'Note',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "about" */ '../views/NoteView.vue')
        // children: [
        //     {
        //         path: "/edit",
        //         name: "Edit Note",
        //         component: () => import(/* webpackChunkName: "about" */ '../views/NoteView.vue')
        //     }
        // ]
    }
    // {
    //   path: '/notes',
    //   name: 'Notes',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ '../views/NotesView.vue')
    // }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
